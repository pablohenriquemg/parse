package com.ef.predicate;

import static org.junit.Assert.assertEquals;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.ef.enumerator.UnitMeasure;
import com.ef.model.LogModel;

public class LogPredicateTest {

	private List<LogModel> listOfLog;
	private DateTimeFormatter formatter = null;
	private ZonedDateTime januaryFist = null;

	@Before
	public void initialize() {
		listOfLog = new ArrayList<>();
		formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		januaryFist = ZonedDateTime.parse("2017-01-01 00:00:00.000", formatter.withZone(ZoneId.systemDefault()))
				.withZoneSameInstant(ZoneOffset.UTC);
	}

	@Test
	public void shouldBeOneLogAfterFilterByHour() {

		listOfLog.add(new LogModel(januaryFist.plusMinutes(10), "192.168.0.10", "'GET / HTTP/1.1'", 200,
				"'swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0'"));
		listOfLog.add(new LogModel(januaryFist.minusDays(1l), "192.168.0.10", "'GET / HTTP/1.1'", 200,
				"'swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0'"));
		List<LogModel> listFiltered = listOfLog.stream()
				.filter(LogPredicate.applyPredicates(januaryFist, UnitMeasure.HOURLY.toString()))
				.collect(Collectors.toList());
		assertEquals(1, listFiltered.size());
	}

	@Test
	public void shouldBeOneLogAfterFilterByDay() {

		listOfLog.add(new LogModel(januaryFist.plusMinutes(30l), "192.168.0.10", "'GET / HTTP/1.1'", 200,
				"'swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0'"));
		listOfLog.add(new LogModel(januaryFist.minusDays(1l), "192.168.0.10", "'GET / HTTP/1.1'", 200,
				"'swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0'"));
		List<LogModel> listFiltered = listOfLog.stream()
				.filter(LogPredicate.applyPredicates(januaryFist, UnitMeasure.DAILY.toString()))
				.collect(Collectors.toList());
		assertEquals(1, listFiltered.size());
	}

}
