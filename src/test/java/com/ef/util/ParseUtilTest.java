package com.ef.util;

import static org.junit.Assert.assertEquals;

import java.time.DayOfWeek;
import java.time.Month;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.ef.model.LogModel;

public class ParseUtilTest {

	@Test
	public void shouldBeFirstDayOfJanuary() {
		String dateString = "2017-01-01 00:00:00.000";
		ZonedDateTime date = ParseUtil.parseStringToZonedDatetime(dateString);
		assertEquals(DayOfWeek.SUNDAY, date.getDayOfWeek());
		assertEquals(Month.JANUARY, date.getMonth());
		assertEquals(1, date.getDayOfYear());
	}

	@Test
	public void shouldBeListOfLog() {
		String[] arr = {
				"2017-01-01 00:00:11.763|192.168.234.82|'GET / HTTP/1.1'|200|'swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0'" };
		List<LogModel> list = ParseUtil.parseLinesToObj(Arrays.asList(arr));
		assertEquals("192.168.234.82", list.get(0).getIpAddress());
	}

}
