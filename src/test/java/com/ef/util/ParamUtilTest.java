package com.ef.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ParamUtilTest {

	@Test
	public void shouldBeDailyParam() throws Exception {
		String entryParam = "--duration=daily";
		String param = ParamUtil.getEntryParam(entryParam, false);
		assertEquals("daily", param);
	}

	@Test
	public void shouldBePathFileParam() throws Exception {
		String entryParam = "--accesslog=C:/access.log";
		String param = ParamUtil.getEntryParam(entryParam, true);
		assertEquals("C:/access.log", param);
	}

	@Test(expected = Exception.class)
	public void shouldBeException() throws Exception {
		String entryParam = "access.log";
		String param = ParamUtil.getEntryParam(entryParam, true);
		assertEquals("C:/access.log", param);
	}

}
