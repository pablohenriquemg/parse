package com.ef.util;

import org.junit.Test;

public class FileUtilTest {

	@Test(expected = Exception.class)
	public void shouldBeNoSuchFile() throws Exception {
		FileUtil.read("C:/file.log");
	}

}
