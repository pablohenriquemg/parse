/*
(1) Write MySQL query to find IPs that mode more than a certain
number of requests for a given time period.
*/
SELECT * FROM mydb.`table`
WHERE `date` >= '2017-01-01 13:00:00'
AND `date` <= '2017-01-01 14:00:00';
LIMIT 100;
/*
(2) Write MySQL query to find requests 
made by a given IP.
*/
SELECT * FROM mydb.`table`
WHERE `ip` = '192.168.234.82';