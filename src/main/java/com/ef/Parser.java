package com.ef;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.ef.model.LogModel;
import com.ef.predicate.LogPredicate;
import com.ef.util.FileUtil;
import com.ef.util.ParamUtil;
import com.ef.util.ParseUtil;

public class Parser {

	public static void main(String[] args) throws Exception {

		// args = new String[4];
		// args[0] ="--accesslog=C:/access.log";
		// args[1] = "--startDate=2017-01-01.00:00:11";
		// args[2] = "--duration=daily";
		// args[3] = "--threshold=100";

		String pathToFile = ParamUtil.getEntryParam(args[0], true);
		ZonedDateTime startDate = ParseUtil
				.parseStringToZonedDatetime(ParamUtil.getEntryParam(args[1], false).concat(".000"));
		String duration = ParamUtil.getEntryParam(args[2], false);
		String threshold = ParamUtil.getEntryParam(args[3], false);

		List<String> linesFromFile = FileUtil.read(pathToFile);
		List<LogModel> listParsed = ParseUtil.parseLinesToObj(linesFromFile);

		List<LogModel> listFiltered = listParsed.stream().filter(LogPredicate.applyPredicates(startDate, duration))
				.limit(Long.parseLong(threshold)).collect(Collectors.toList());

		listFiltered.forEach(item -> {
			System.out.println(item.toString());
		});
		System.out.println("List size = " + listFiltered.size());
	}

}
