package com.ef.model;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class LogModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 965266301159267047L;

	private ZonedDateTime date;
	private String ipAddress;
	private String request;
	private Integer status;
	private String userAgent;

	public LogModel() {
		super();
	}

	public LogModel(ZonedDateTime date, String ipAddress, String request, Integer status, String userAgent) {
		super();
		this.date = date;
		this.ipAddress = ipAddress;
		this.request = request;
		this.status = status;
		this.userAgent = userAgent;
	}

	public String toString() {
		return "Ip address= " + this.ipAddress;
	}

	public ZonedDateTime getDate() {
		return date;
	}

	public void setDate(ZonedDateTime date) {
		this.date = date;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

}
