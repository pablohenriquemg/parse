package com.ef.enumerator;

public enum UnitMeasure {

	HOURLY("hourly"), DAILY("daily");

	private final String text;

	/**
	 * @param text
	 */
	private UnitMeasure(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}
}
