package com.ef.util;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FileUtil {

	@SuppressWarnings("resource")
	public static List<String> read(String pathToFile) throws Exception {

		List<String> list = new ArrayList<>();
		Stream<String> stream = null;
		try {
			stream = Files.lines(Paths.get(pathToFile));
			stream.forEach(line -> {
				list.add(line);
			});
		} catch (Exception e) {
			throw new Exception(e);
		}
		return list;
	}

}
