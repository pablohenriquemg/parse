package com.ef.util;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.ef.model.LogModel;

public class ParseUtil {

	public static List<LogModel> parseLinesToObj(List<String> linesFromFile) {
		List<LogModel> list = new ArrayList<>();
		linesFromFile.forEach(line -> {

			String[] properties = line.split(Pattern.quote("|"));
			list.add(new LogModel(parseStringToZonedDatetime(properties[0]), properties[1], properties[2],
					Integer.parseInt(properties[3]), properties[4]));

		});
		return list;
	}

	public static ZonedDateTime parseStringToZonedDatetime(String stringToParse) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		return ZonedDateTime.parse(stringToParse, formatter.withZone(ZoneId.systemDefault()))
				.withZoneSameInstant(ZoneOffset.UTC);
	}
}
