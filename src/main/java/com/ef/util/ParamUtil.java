package com.ef.util;

public class ParamUtil {

	public static String getEntryParam(String entryParam, boolean ignoreDot) throws Exception {
		try {
			String[] dateParam = entryParam.split("\\=");
			if (ignoreDot) {
				return dateParam[1];
			}
			return dateParam[1].replace(".", " ");
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
