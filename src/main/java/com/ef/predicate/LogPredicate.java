package com.ef.predicate;

import java.time.ZonedDateTime;
import java.util.function.Predicate;

import com.ef.enumerator.UnitMeasure;
import com.ef.model.LogModel;

public class LogPredicate {

	public static Predicate<LogModel> applyPredicates(ZonedDateTime startDate, String duration) {
		
		return log -> {
			
			ZonedDateTime endDate = startDate;
			if (UnitMeasure.HOURLY.toString().equals(duration)) {
				endDate = endDate.plusHours(1l);
			}

			if (UnitMeasure.DAILY.toString().equals(duration)) {
				endDate = endDate.plusDays(1l);
			}

			return log.getDate().isAfter(startDate) && log.getDate().isBefore(endDate);
		};
	}
}
