CREATE TABLE IF NOT EXISTS `mydb`.`table` (
  `idtable` INT NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL,
  `ip` VARCHAR(15) NOT NULL,
  `request` VARCHAR(25) NOT NULL,
  `status` INT NOT NULL,
  `user_agent` TEXT(200) NOT NULL,
  PRIMARY KEY (`idtable`))
ENGINE = InnoDB